FROM php:apache-buster
RUN apt-get update && apt-get upgrade -y
RUN apt-get install zlib1g-dev libpng-dev libonig-dev -y

# Copy apache vhost file to proxy php requests to php-fpm container
COPY ./apache/sidb.apache.conf /usr/local/apache2/conf/sidb.apache.conf
RUN echo "Include /usr/local/apache2/conf/sidb.apache.conf" >> /usr/local/apache2/conf/httpd.conf
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install gd
RUN docker-php-ext-install mbstring
#RUN docker-php-ext-enable mysqli
#RUN docker-php-ext-enable gd
#RUN docker-php-ext-enable mbstring
