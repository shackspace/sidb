-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Erstellungszeit: 28. Okt 2020 um 16:07
-- Server-Version: 10.5.6-MariaDB-1:10.5.6+maria~focal
-- PHP-Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `sidb`
--
CREATE DATABASE IF NOT EXISTS `sidb` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sidb`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `asset`
--

CREATE TABLE `asset` (
  `idasset` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `serialno` varchar(20) DEFAULT NULL,
  `location` text CHARACTER SET latin1 DEFAULT NULL,
  `docuurl` varchar(200) DEFAULT NULL,
  `assettype_idassettype` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT 1,
  `asset_idasset` int(11) DEFAULT NULL COMMENT 'Parent',
  `ownerstate_idownerstate` int(11) NOT NULL,
  `owner_idowner` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  `created` datetime NOT NULL,
  `last-changed` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `asset`
--

INSERT INTO `asset` (`idasset`, `name`, `description`, `serialno`, `location`, `docuurl`, `assettype_idassettype`, `count`, `asset_idasset`, `ownerstate_idownerstate`, `owner_idowner`, `deleted`, `created`, `last-changed`) VALUES
(1, 'HQ', 'Dies ist der Startpunkt von allem', '', '', '', 1, 1, NULL, 1, 1, 0, '2020-10-28 16:04:38', '2020-10-28 16:04:38');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `assetgrant`
--

CREATE TABLE `assetgrant` (
  `idassetgrant` int(11) NOT NULL,
  `asset_idasset` int(11) NOT NULL,
  `owner_idowner` int(11) NOT NULL,
  `allow_instructing` tinyint(1) NOT NULL DEFAULT 0,
  `allow_edit` tinyint(1) NOT NULL DEFAULT 0,
  `allow_post` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `assettype`
--

CREATE TABLE `assettype` (
  `idassettype` int(11) NOT NULL,
  `typename` varchar(100) CHARACTER SET latin1 NOT NULL,
  `consumable` tinyint(1) NOT NULL DEFAULT 0,
  `childpossible` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `assettype`
--

INSERT INTO `assettype` (`idassettype`, `typename`, `consumable`, `childpossible`) VALUES
(1, 'Gebäude', 0, 1),
(2, 'Räume', 0, 1),
(3, 'Server', 0, 1),
(4, 'Fächer', 0, 1),
(5, 'Boxen', 0, 1),
(6, 'Bauteile - passiv', 1, 0),
(7, 'Bauteile - aktiv', 1, 0),
(8, 'Netzwerkdevice', 0, 0),
(9, 'Schränke/Regale', 0, 1),
(10, 'Undefined', 0, 1),
(11, 'Lebensmittel', 1, 0),
(12, 'Non Food (Verbrauchsmaterial)', 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `log`
--

CREATE TABLE `log` (
  `idlog` int(11) NOT NULL,
  `asset_idasset` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `countchange` int(11) NOT NULL DEFAULT 0,
  `action` text CHARACTER SET latin1 NOT NULL,
  `owner_idowner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `log`
--

INSERT INTO `log` (`idlog`, `asset_idasset`, `datetime`, `countchange`, `action`, `owner_idowner`) VALUES
(1, 1, '2020-10-28 16:04:38', 1, 'Asset created.', 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `owner`
--

CREATE TABLE `owner` (
  `idowner` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `ownergroup_idownergroup` int(11) NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `owner`
--

INSERT INTO `owner` (`idowner`, `username`, `password`, `ownergroup_idownergroup`, `blocked`) VALUES
(1, 'shack', '$argon2id$v=19$m=65536,t=4,p=1$a1FqSlRxQUZyMDhxRlgzVQ$OGGURpsKYY/tpCZFXBjiaaZJYP3amBmyu6cD5KS34xA', 1, 0),
(2, 'exec', '$argon2id$v=19$m=65536,t=4,p=1$WlJBTXNsS2U0VlZnSndFcg$AvVXXuQs2BxfaAXcwW61xLOnzR3ILCZIH4xMMphD72c', 1, 0),
(3, 'hase', '$argon2id$v=19$m=65536,t=4,p=1$UkNVMlVXUm4wYVdNb3U1aw$hBUKNBxiKxC012B7x0NsaMxGZMUWvjnaVo9d85Xs/jA', 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `ownergroup`
--

CREATE TABLE `ownergroup` (
  `idownergroup` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `edit_owner` tinyint(1) NOT NULL DEFAULT 0,
  `edit_assetcategory` tinyint(1) NOT NULL DEFAULT 0,
  `edit_owningstate` tinyint(1) NOT NULL DEFAULT 0,
  `show_all_assets` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Override Owningstate viewowneronly',
  `edit_all_assets` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `ownergroup`
--

INSERT INTO `ownergroup` (`idownergroup`, `name`, `edit_owner`, `edit_assetcategory`, `edit_owningstate`, `show_all_assets`, `edit_all_assets`) VALUES
(1, 'Admin', 1, 1, 1, 1, 1),
(2, 'User', 0, 0, 0, 1, 0),
(3, 'Benutzeradmin', 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `owningstate`
--

CREATE TABLE `owningstate` (
  `idowningstate` int(11) NOT NULL,
  `state` varchar(11) CHARACTER SET latin1 NOT NULL,
  `printtext` text NOT NULL,
  `viewowneronly` int(11) NOT NULL DEFAULT 0,
  `color1` varchar(7) CHARACTER SET latin1 NOT NULL,
  `color2` varchar(7) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `owningstate`
--

INSERT INTO `owningstate` (`idowningstate`, `state`, `printtext`, `viewowneronly`, `color1`, `color2`) VALUES
(1, 'Public', 'Nutzung für alle erlaubt.', 0, '#36BC03', '#36BC03'),
(2, 'Give Away', 'Zum Mitnehmen!', 0, '#36BC03', '#FFFFFF'),
(3, 'Documented', 'Nur nach Anleitung zu benutzen / verbrauchen.', 0, '#FEFF00', '#FEFF00'),
(4, 'Instructed', 'Nur nach Einweisung zu benutzen / verbrauchen.', 0, '#F50B00', '#FEFF00'),
(5, 'Owner Only', 'Nur mit ausdrücklicher Genehmigung des Eigentümers zu benutzen / verbrauchen.', 1, '#F50B00', '#F50B00'),
(6, 'Unknown', 'Nach Ablauf von 6 Monaten geht es in das Eigentum des Shacks über', 0, '#6666FF', '#FFFFFF');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`idasset`);
ALTER TABLE `asset` ADD FULLTEXT KEY `name` (`name`,`description`);

--
-- Indizes für die Tabelle `assetgrant`
--
ALTER TABLE `assetgrant`
  ADD PRIMARY KEY (`idassetgrant`);

--
-- Indizes für die Tabelle `assettype`
--
ALTER TABLE `assettype`
  ADD PRIMARY KEY (`idassettype`);

--
-- Indizes für die Tabelle `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idlog`);

--
-- Indizes für die Tabelle `owner`
--
ALTER TABLE `owner`
  ADD UNIQUE KEY `idowner` (`idowner`);

--
-- Indizes für die Tabelle `ownergroup`
--
ALTER TABLE `ownergroup`
  ADD PRIMARY KEY (`idownergroup`);

--
-- Indizes für die Tabelle `owningstate`
--
ALTER TABLE `owningstate`
  ADD PRIMARY KEY (`idowningstate`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `asset`
--
ALTER TABLE `asset`
  MODIFY `idasset` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `assetgrant`
--
ALTER TABLE `assetgrant`
  MODIFY `idassetgrant` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `assettype`
--
ALTER TABLE `assettype`
  MODIFY `idassettype` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT für Tabelle `log`
--
ALTER TABLE `log`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT für Tabelle `owner`
--
ALTER TABLE `owner`
  MODIFY `idowner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `ownergroup`
--
ALTER TABLE `ownergroup`
  MODIFY `idownergroup` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `owningstate`
--
ALTER TABLE `owningstate`
  MODIFY `idowningstate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
