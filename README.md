Shack Inventory DataBase
=====

Die SIDB wird ein Assetmanagement mit erweiterten Funktionen wie in MicroWaWi etc. 

Aktueller stand der Entwicklung kann über das [Kanboard](http://sidb.execnet.de/kanboard/?controller=BoardViewController&action=readonly&token=7aaf1bad60b4658623e1f50fb81e548eecd11a8cb04cf00a4e5a11aec38a) angesehen werden.

# Installation mit Docker

## 1. Installiere GIT und Docker

Installieren wie für dein Betriebssystem vorgesehen

## 2. Klone das gitlab repo

```
git clone https://gitlab.com/shackspace/sidb.git
```

## 3. starte das Docker-Compose

```
docker-compose up -d
```

## 4. installiere die Datenbank auf dem mysql-Server

Dies geht über PHPMyAdmin ganz einfach. [PHPMyAdmin](http://localhost:81/)

```
Benutzername: root
Passwort: deinemutter
```

Import-Reiter Datei "sidb.sql" und unten auf OK klicken

## 5. Webseite aufrufen und los legen

los geht es mit folgenden Link: [SIDB](http://localhost)

Aktuell gibt es noch keinen Initial-User. 
