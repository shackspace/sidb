<main><?php 
if (!isset($_SESSION['currentpage'])) $_SESSION['currentpage'] = "start";
//$DebugOutput .= "P bch: ".$_SESSION['currentpage']."<br>";
if (isset($_GET['page'])) $_SESSION['currentpage'] = $_GET['page'];
if (isset($_POST['page'])) $_SESSION['currentpage'] = $_POST['page'];
if ($_SESSION['currentpage'] == "") $_SESSION['currentpage'] = "start";
//$DebugOutput .= "P ach: ".$_SESSION['currentpage']."<br>";
$page = $_SERVER['DOCUMENT_ROOT']."/inc/pages/".$_SESSION['currentpage'];
if (file_exists($page.".html")) $page .= ".html";
if (file_exists($page.".htm")) $page .= ".htm";
if (file_exists($page.".php")) $page .= ".php";
//$DebugOutput .= "P Path: ".$page."<br>";
if (!file_exists($page)) $page = "pages/404.html";
include($page); ?></main>