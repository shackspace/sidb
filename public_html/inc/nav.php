<!-- General Nav -->
<?php
if (isset($_SESSION['currentpage']) | isset($_GET['page'])) $p_act = (isset($_GET['page']) ? $_GET['page'] : $_SESSION['currentpage']); ?>
<nav class="nav_main">
    <ul class="nav_vertical_left">
        <a href="?page=start"><li class="<?php echo ($p_act == "start" ? "active":""); ?>">Start</li></a>
        <a href="?page=assets"><li class="<?php echo ($p_act == "assets" ? "active":""); ?>">Assets</li></a>
        <?php if ($_SESSION['r_e_owner']) { ?><a href="?page=ownerlist"><li class="<?php echo ($p_act == "ownerlist" ? "active":""); ?>">Benutzer</li></a><?php } ?>
        <?php if ($_SESSION['r_e_owningstate'] | $_SESSION['r_e_assetcategory']) { ?><a href="?page=config"><li class="<?php echo ($p_act == "config" ? "active":""); ?>">Konfiguration</li></a><?php } ?>
        <a href="?page=showselectedassets"><li id="selectnav" style="display: <?php echo (isset($_SESSION['assetselectlist']) ? "block" : "none"); ?>;" class="<?php echo ($p_act == "showselectedassets" ? "active":""); ?>">Auswahl (<?php echo count($_SESSION['assetselectlist']); ?>) nutzen</li></a>
        <?php if (isset($_SESSION['ownerid'])) { ?>
        <a href="?page=userchange"><li class="<?php echo ($p_act == "userchange" ? "active":""); ?>">Benutzer ändern</li></a>
        <a href="?logout"><li>Logout</li></a>
        <?php } else { ?>
        <a href="?page=userchange"><li class="<?php echo ($p_act == "userchange" ? "active":""); ?>">Registrieren</li></a>
        <li><?php include('inc/forms/login.php'); ?></li>
        <?php } ?>
    </ul>
    <?php if ($debug) { ?>
    <a href="/?debug=0#">DEBUG OFF</a>
    <?php } else { ?>
    <a href="/?debug=1#">DEBUG ON</a>
    <?php } ?>
</nav>

<!-- Asset Nav -->
<!--nav class="nav_asset" style="">
    <ul class="nav_vertical_left">
        <a href="?page=start"><li>HQ</li></a>
        <a href="?page=assets"><li>Raum1</li></a>
    </ul>
</nav-->
<?php 
