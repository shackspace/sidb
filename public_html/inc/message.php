<?php 
$show_w = $show_e = $show_bg = false;
if ($UserErrorOutput != "") $show_bg = $show_e = true;
if ($UserInfoOutput != "") $show_bg = $show_w = true;
?>
<style>
    #message_bg {
        <?php if ($show_bg) echo "display: block;"; else echo "display: none;"; ?>
        background-color: rgba(0,0,0,0.5);
        background-image: url("img/overlay_bg_1_33pr.png");
        position: fixed;
        height: 100%;
        width: 100%;
        z-index: 10000;    
    }

    #message_info {
        <?php if ($show_w) echo "display: block;"; else echo "display: none;"; ?>
        background-color: #9f9;
        color: black;
        position: fixed;
        width: 40%;
        margin-left: 30%;
        margin-top: 10%;
        padding: 20px;
        border: 3px solid darkgreen;
        border-radius: 10px;
        box-shadow: 5px 5px 30px 15px black;
    }

    #message_error {
        <?php if ($show_e) echo "display: block;"; else echo "display: none;"; ?>
        background-color: #f99;
        color: black;
        position: fixed;
        width: 40%;
        margin-left: 30%;
        margin-top: 10%;
        padding: 20px;
        border: 3px solid darkred;
        border-radius: 10px;
        box-shadow: 5px 5px 30px 15px black;
    }
</style>
<div id="message_bg">
    <div id="message_info" onclick="document.getElementById('message_bg').style.display = 'none';"><H1>Information</H1><?php echo $UserInfoOutput; ?><br><div style="font-size: small; text-align: center; margin-top: 10px;">klick mich zum schließen</div></div>
    <div id="message_error" onclick="document.getElementById('message_bg').style.display = 'none';"><H1>Fehler</H1><?php echo $UserErrorOutput; ?><br><div style="font-size: small; text-align: center; margin-top: 10px;">klick mich zum schließen</div></div>
</div>