<?php 
// Fremde Libs 
require($_SERVER['DOCUMENT_ROOT'].'/libs/phpqrcode/qrlib.php');
require($_SERVER['DOCUMENT_ROOT'].'/config.php');

if (isset($_GET['qrtext'])) {
    QRcode::png($_GET['qrtext'], false, QR_ECLEVEL_L, 1);
} else if (isset($_GET['assetid'])) {
    QRcode::png($serverurl.'?page=asset_detail&assetid='.$_GET['assetid'], false, QR_ECLEVEL_H, 2);
} else {
    echo "WHAT?!?";
}