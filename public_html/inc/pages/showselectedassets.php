<h1>Auswahl benutzen</h1>
<?php 
if (!isset($_SESSION['assetselectlist'])) {
    ?><h2>Keine Auswahl, keine Anzeige</h2>
    <p>Es scheint so, als ob du auf diese Seite gelangt bist, ohne eine Auswahl getroffen zu sein. Daher gibt es hier nichts zu sehen. </p>
    <p>Dies kann auch durch eine abgelaufene Session passieren</p>
    <?php 
} else {
$sql = "SELECT * FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype)";
$first = true;

foreach ($_SESSION['assetselectlist'] as $key => $value) {
    if ($first) {
        $sql .= " WHERE `idasset` = '".$value."'";
        $first = false;
    } else $sql .= " OR `idasset` = '".$value."'";
}

if($res = $mysqli_ro->query($sql))
{
    $arr = $res->fetch_all(MYSQLI_ASSOC);
} else {
    echo "ERROR [".$mysqli_ro->errno."] ".$mysqli_ro->error;
    echo "<br>".$sql;
}
?>

<table>
    <tr>
        <th></th>
        <th>ID</th>
        <th>Name</th>
        <th>Owner</th>
        <th>Status</th>
        <th>Serialno</th>
        <th>Dokulink</th>
    <tr>
<?php 
foreach ($arr as $key => $value) {
    ?>
    <tr>
        <td></td>
        <td><?php echo $value['idasset']; ?></td>
        <td><?php echo $value['name']; ?></td>
        <td><?php echo $value['username']; ?></td>
        <td><?php echo $value['state']; ?></td>
        <td><?php echo (strlen($value['serialno']) > 0 ? $value['serialno'] : "- nicht vorhanden -"); ?></td>
<td><?php if (strlen($value['docuurl']) > 5) { ?><img src="/inc/qrcode.php?qrtext=<?php echo urlencode($value['docuurl']); ?>"><?php } else echo "- nicht vorhanden - "; ?></td>
    <tr>
    <?php
}

?>
</table>

<a href="/pdf/maxilabel.php" target="_blank"><button>Erzeuge Labels</button></a>
<a href="/pdf/microlabel.php" target="_blank"><button>Erzeuge Microlabels</button></a>
<a href="?f=deselect_all_assets&page=assets"><button>Leere Auswahl</button></a>


<?php } ?>