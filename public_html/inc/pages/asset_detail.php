<h1>Assetdetails</h1>

<?php
if (!isset($_GET['assetid'])) {
    ?><h2>Ups..</h2>
    Leider ist hier ein Fehler aufgetretten und es sind nicht alle Informationen verfügbar.<br>
    Bitte versuche es noch einmal.<?php
} else {
$sql_a = "SELECT * FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype) where idasset = ".$_GET['assetid'];
$sql_l = "SELECT l.* FROM `log` as l WHERE `asset_idasset` = ".$_GET['assetid']." ORDER BY datetime ASC";
$arr_a = $mysqli_ro->query($sql_a)->fetch_array(MYSQLI_ASSOC);
$arr_l = $mysqli_ro->query($sql_l)->fetch_all(MYSQLI_ASSOC);
?>

<?php if (isset($_SESSION['ownerid']) && ($arr_a['owner_idowner'] == $_SESSION['ownerid'] | $_SESSION['r_e_all_assets'] == true)) { ?> 
<a href="?page=asset_form&assetid=<?php echo $arr_a['idasset']; ?>"><button class="text_icon_button" title="Bearbeiten">E</button></a>
<?php } 
if ($arr_a['childpossible'] == 1) { ?>
        <a href="?page=assets&subasset=<?php echo $arr_a['idasset']; ?>"><button class="text_icon_button" title="Zu Kinderliste wechseln">L</button></a>
<?php } ?>
<?php if(isset($_SESSION['ownerid'])) { ?><a href="?page=asset_detail&f=toggle_select_asset&hideoutput&assetid=<?php echo $arr_a['idasset']; ?>"><button class="text_icon_button <?php if (isset($_SESSION['assetselectlist'])) echo (in_array($arr_a['idasset'], $_SESSION['assetselectlist']) ? "selected" : "unselected"); ?>" title="Selektieren">S</button></a><?php } ?>
        <table>
    <tr>
        <th>Name</th><td><?php echo $arr_a['name']; ?></td>
        <th>Ort</th><td><?php echo $arr_a['location']; ?></td>
    </tr>
    <tr>
        <th>Beschreibung</th><td colspan="3"><?php echo $arr_a['description']; ?></td>
    </tr>
    <tr>
        <th>Dokumentation</th>
        <td colspan="3">
            <?php if (strlen($arr_a['docuurl'])) { ?>
            <a href="<?php echo $arr_a['docuurl']; ?>" target="_blank"><?php echo $arr_a['docuurl']; ?></a>
            <?php } else echo "- keine Angegeben -"; ?>
        </td>
    </tr>
    <tr>
        <th>Anzahl vorrätig</th><td><?php echo $arr_a['count']; ?></td>
        <th>Seriennummer</th><td colspan="3"><?php echo $arr_a['serialno']; ?></td>
    </tr>
    <tr>
        <th>Typ</th><td><?php echo $arr_a['typename']; ?></td>
        <th>Sichtbarkeit</th><td><?php echo ($arr_a['viewowneronly'] ? "Persönlich und Admins" : "Public"); ?></td>
    </tr>
    <tr>
        <th>Erstellt</th><td><?php echo $arr_a['created']; ?></td>
        <th>Letzte Änderung</th><td><?php echo $arr_a['last-changed']; ?></td>
    </tr>
    <tr>
        <th>Status</th>
        <td>
            <div style="background-color: <?php echo $arr_a['color1']; ?>;width:20px;height:20px;display: inline-block; border-radius: 100% 0% 0% 100%;"></div> <div style="background-color: <?php echo $arr_a['color2']; ?>;width:20px;height:20px;display: inline-block; border-radius: 0% 100% 100% 0%;"></div>
            <?php echo $arr_a['state']; ?></td>
        <th>Owner</th><td><?php echo "[".$arr_a['idowner']."] ".$arr_a['username']; ?></td>
    </tr>
    <tr>
        <th>Labels</th>
        <td colspan="3">
            <a href="/pdf/maxilabel.php?singlelabel=<?php echo $arr_a['idasset']; ?>" target="_blank"><button>Erzeuge Label</button></a>
            <a href="/pdf/microlabel.php?singlelabel=<?php echo $arr_a['idasset']; ?>" target="_blank"><button>Erzeuge Microlabel</button></a>
        </td>
    </tr>
</table>
<?php //echo arr_tostr($arr_a);
?>
<h3>Lagerort</h3>
<?php
$parent_id = $arr_a['asset_idasset'];
$arr_loc = array();
while ($parent_id != NULL) {
    $sql_pa = "SELECT * FROM `asset` WHERE idasset = ".$parent_id;
    $arr_pa = $mysqli_ro->query($sql_pa)->fetch_array(MYSQLI_ASSOC);
    $parent_id = $arr_pa['asset_idasset'];
    $arr_loc[] = $arr_pa;
    //echo arr_tostr($arr_loc);
}
krsort($arr_loc);
foreach($arr_loc as $loc) { ?>
    <a href="?page=asset_detail&assetid=<?php echo $loc['idasset']; ?>"><div class="locationitem" title="<?php echo $loc['location']; ?>"><?php echo $loc['name']; ?></div></a>
<?php } ?>
<?php
//echo arr_tostr($arr_loc);
?>
<?php if ($arr_a['consumable']) { ?>
<h3>Stückbuchung</h3>
<div class="DEVNOTE">wird noch Entwickelt. Dieses Asset hätte diese Option.</div>
<table>
    <tr>
        <th>Buchung</th>
        <td>
            <select>
                <option value="neg">[Entnahme] habe für eine Projekt entnommen</option>
                <option value="pos">[Spende] Ich habe gespendet</option>
            </select>
        </td>
    <tr>
    <tr>
        <th>Anzahl</th>
        <td>
            <input type="number" min="1">
        </td>
    <tr>
    <tr>
        <th></th>
        <td>
            <input type="submit" value="Buchen">
        </td>
    <tr>
</table>
<?php } ?>
<h3>Assetverlauf (Logbuch)</h3>
<table>
    <tr>
        <th>Zeitstempel</th>
        <th>Person</th>
        <th>Aktion</th>
        <?php if ($arr_a['consumable']) { ?><th>Buchung</th><?php } ?>
    </tr>
<?php foreach ($arr_l as $row)  {  ?>
    <tr>
        <td><?php echo $row['datetime']; ?></td>
        <td><?php echo "ID: ".$row['owner_idowner']; ?></td>
        <td><?php echo $row['action']; ?></td>
        <?php if ($arr_a['consumable']) { ?><td><?php echo $row['countchange']; ?></td><?php } ?>
    </tr>
<?php } ?>
</table>

<?php } ?>
