<h1>Willkommen in der SIDB</h1>
<p>Dies ist das Inventory von Stuttgarter Hackerspace. Es wurde entwickelt um alle Gegenstände zu erfassen und dementsprechend auch zu Suchen. Da auch immer wieder die Frage kommt, Wemn gehört was und wer darf was wie wo tun, wird dies ebenfalls mit erfasst. </p>
<p>Wir bitte darum alle Teile, die im Shack sind bzw. kommen hier zu erfassen. Am wichtigsten sind uns die Assets die nicht mit "Owner Only" gekennzeichnet sind. Es wäre jedoch schön dies ebenfalls hier in diesem System zu erfassen, damit Ihr und auch alle anderen einen Überblick haben.</p>
<div class="DEVNOTE">
<p>Aktuell ist das System noch in der Entwicklung und es sind noch nicht alle Funktionen die hier dargestellt werden auch benutzbar oder führen zu Fehlern.<br>LG exec<p>

<p><a href="http://sidb.execnet.de/kanboard/?controller=BoardViewController&action=readonly&token=7aaf1bad60b4658623e1f50fb81e548eecd11a8cb04cf00a4e5a11aec38a">Link zum Kanban</a></p>
</div>
