<h1>Assetformular</h1>
<?php 
$sql_at = "SELECT * FROM `assettype` order by typename";
$sql_o = "SELECT * FROM `owner` order by username";
$sql_os = "SELECT * FROM `owningstate` order by state";
$sql_ak = "SELECT * FROM `asset`";

$arr_at = $mysqli_ro->query($sql_at)->fetch_all(MYSQLI_ASSOC);
$arr_o = $mysqli_ro->query($sql_o)->fetch_all(MYSQLI_ASSOC);
$arr_os = $mysqli_ro->query($sql_os)->fetch_all(MYSQLI_ASSOC);
$arr_ak = $mysqli_ro->query($sql_ak)->fetch_all(MYSQLI_ASSOC);

$function = "";
if (isset($_GET['parent_assetid'])) {
    $a_id = 0;                                // Asset default id
    $a_name = "";                           // Assetname default
    $a_desc = "";                          // Beschreibung default
    $a_loc = "";                            // Ort default
    $a_sno = "";                            // Seriennummer default
    $a_doc = "";                            // Documentation default
    $a_count = 1;                           // Anzahl default
    $a_at = $standart_assettypeid;                              // Assettype default id
    $a_os = $standart_owningstateid;                              // Owningstate default id
    $a_o = (isset($_SESSION['ownerid']) ? $_SESSION['ownerid'] : $standart_ownerid);            // Owner default id
    $a_ea = $_GET['parent_assetid'];        // Eltern id
    $function = "new_asset";                // Submit Operation name
}

if (isset($_GET['assetid'])) {
    $sql_a = "SELECT * FROM `asset` where idasset = ".$_GET['assetid'];
    $arr_a = $mysqli_ro->query($sql_a)->fetch_array(MYSQLI_ASSOC);

    $a_id = $arr_a['idasset'];                                // Asset default id
    $a_name = $arr_a['name'];                           // Assetname default
    $a_desc = $arr_a['description'];                           // Beschreibung default
    $a_loc = $arr_a['location'];                             // Ort default
    $a_sno = $arr_a['serialno'];                            // Seriennummer default
    $a_doc = $arr_a['docuurl'];                            // Documentation default
    $a_count = $arr_a['count'];                            // Anzahl default
    $a_at = $arr_a['assettype_idassettype']; ;                              // Assettype default id
    $a_os = $arr_a['ownerstate_idownerstate'];                               // Owningstate default id
    $a_o = $arr_a['owner_idowner'];             // Owner default id
    $a_ea = $arr_a['asset_idasset'];         // Eltern id
    $function = "edit_asset";
}

if ($function != "") { 
?>


<form>
    <table>
        <tr>
            <th>Assetname *</th>
            <td>
                <input name="a_name" required="required" value="<?php echo $a_name; ?>" />
                <?php if ($function == "edit_asset") { ?>
                    <input type="hidden" name="page" value="assets" />
                <?php } ?>
                <input type="hidden" name="f" value="<?php echo $function; ?>" />
                <input type="hidden" name="a_id" value="<?php echo $a_id; ?>" />
            </td>
        </tr>
        <tr><th>Beschreibung</th><td><textarea name="a_desc"><?php echo $a_desc; ?></textarea></td></tr>
        <tr><th>Ort</th><td><input name="a_loc" value="<?php echo $a_loc; ?>" /></td></tr>
        <tr><th>Seriennummer</th><td><input name="a_sno" value="<?php echo $a_sno; ?>" /></td></tr>
        <tr><th>Dokumentation</th><td><input type="url" name="a_doc" value="<?php echo $a_doc; ?>" /></td></tr>
        <tr><th>Anzahl</th><td><input type="number" name="a_count" value="<?php echo $a_count; ?>" min="1" /></td></tr>
        <tr>
            <th>Asset Typ</th>
            <td>
                <select name="a_at">
                    <?php foreach ($arr_at as $row)  { ?>
                    <option value="<?php echo $row['idassettype']; ?>" <?php echo ($a_at == $row['idassettype'] ? "selected=\"selected\"": ""); ?>><?php echo $row['typename']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <th>Ownerstate</th>
            <td>
                <select name="a_os">
                    <?php foreach ($arr_os as $row)  { ?>
                    <option value="<?php echo $row['idowningstate']; ?>" <?php echo ($a_os == $row['idowningstate'] ? "selected=\"selected\"": ""); ?>><?php echo $row['state']; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <tr>
            <th rowspan="2">Owner</th>
            <td>
                <select name="a_o" id="a_o">
                    <?php 
                    $list_index = 0;
                    foreach ($arr_o as $row)  { ?>
                    <option value="<?php echo $row['idowner']; ?>" <?php echo ($a_o == $row['idowner'] ? "selected=\"selected\"": ""); ?>><?php echo "[".$row['idowner']."] ".$row['username']; ?></option>
                    <?php 
                    if ($standart_ownerid == $row['idowner']) $a_o_stdowner_i = $list_index;
                    if ($_SESSION['ownerid'] == $row['idowner']) $a_o_owner_i = $list_index;
                    $list_index++;
                    } ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><a onclick="document.getElementById('a_o').selectedIndex = <?php echo $a_o_owner_i; ?>; this.preventDefault();">Mich als Owner</a> | 
                <a onclick="document.getElementById('a_o').selectedIndex = <?php echo $a_o_stdowner_i; ?>; this.preventDefault();">Shack als Owner</a></td>
        </tr>
        <tr>
            <th rowspan="2">Eltern ID</th>
            <td>
                <input type="number" id="a_ea_dis" name="a_ea_disabled" value="<?php echo $a_ea; ?>" min="1" disabled="disabled" />
                <input type="hidden" id="a_ea_hid" name="a_ea" value="<?php echo $a_ea; ?>" />
                <input type="hidden" name="parent_assetid" value="<?php echo $a_ea; ?>" />
            </td>
        </tr>
        <tr>
            <td><div class="DEVNOTE">UMZUG in ein anderes Eltern-Asset wird hier gerade entwickelt</div>
            <div id="parentinserts"></div>
            </td>
        </tr>
        <tr>
            <th></th>
            <td>
                <input type="submit" />
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
    var output_div = document.getElementById('parentinserts');
    var output_id1 = document.getElementById('a_ea_dis');
    var output_id2 = document.getElementById('a_ea_hid');
    
    function change_parent_click(new_parentid) {
        output_div.innerHTML = "<h3>Bitte warten, Daten werden geladen...</h3>";
        var req = new XMLHttpRequest();
        req.overrideMimeType("application/json");
        req.open('GET', "/functions.php?f=json_asset_childposible&id="+new_parentid, true);
        req.onload  = function() {
            var jsonResponse = JSON.parse(req.responseText);
            //document.getElementById('debugjson').innerHTML = jsonResponse;
            console.log(jsonResponse);
            output_id1.value = jsonResponse['currentid'];
            output_id2.value = jsonResponse['currentid'];
            output_div.innerHTML = "<a onclick='change_parent_click(1)'><div class='locationitem'>Ganz Oben</div></a> ";
            output_div.innerHTML = "";
            var i = 0;
            console.log("Output Parents:");
            for (let i=0; i<jsonResponse['parents'].length; i++) {
                var ca = document.createElement("a");
                var cd = document.createElement("div");
                cd.className = "locationitem";
                cd.innerHTML = jsonResponse['parents'][i]['name'];
                ca.title = jsonResponse['parents'][i]['description'];
                ca.appendChild(cd);
                output_div.appendChild(ca);
                output_div.appendChild(document.createTextNode(" "));
                ca.addEventListener("click", function(){ change_parent_click(jsonResponse['parents'][i]['idasset'])});
                console.log(jsonResponse['parents'][i]);
            };
            output_div.appendChild(document.createElement("hr"));
                        i=0;
            console.log("Output Childs:");
            for (let i=0; i<jsonResponse['childs'].length; i++) {
                var ca = document.createElement("a");
                var cd = document.createElement("div");
                cd.className = "locationitem";
                cd.innerHTML = jsonResponse['childs'][i]['name'];
                ca.title = jsonResponse['childs'][i]['description'];
                ca.appendChild(cd);
                output_div.appendChild(ca);
                output_div.appendChild(document.createTextNode(" "));
                ca.addEventListener("click", function(){ change_parent_click(jsonResponse['childs'][i]['idasset'])});
                console.log(jsonResponse['childs'][i]);
            };
        };
        req.send(null);
    }
    
    change_parent_click(<?php echo $a_ea; ?>);
    
    </script>
<?php } else { ?>
<h2>Da isch aber was schief ganga</h2>
<?php } ?>