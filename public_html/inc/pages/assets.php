
<h1>Assetliste</h1>
<?php 




$pagecount = 0;
$search = "";
$subasset = 0;
$ordercol = "name";
$linesperpage = 20;
$show_cards = false;
if (!isset($_SESSION['asset_card_show'])) {
    $_SESSION['asset_card_show'] = $show_cards;
}
if (isset($_GET['cards'])) {
    $_SESSION['asset_card_show'] = ($_GET['cards'] == 1 ? true : false);
}
$show_cards = $_SESSION['asset_card_show'];

if (isset($_GET['pagecount'])) $pagecount = $_GET['pagecount'];
if (isset($_GET['search'])) $search = $_GET['search'];
if (isset($_GET['subasset'])) $subasset = $_GET['subasset'];
if (isset($_GET['ordercol'])) $ordercol = $_GET['ordercol'];
$sql = "";
if ($search != "")
{
    $sql = "SELECT * FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype) WHERE a.idasset = '".$search."' UNION ";
}
$sql .= "(SELECT * FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype)";
if ($search != "")
{
    $sql .= " WHERE a.name LIKE '%".$search."%' OR a.description LIKE '%".$search."%'";
    //$sql .= " WHERE MATCH (a.name, a.description) AGAINST ('".$search."' IN BOOLEAN MODE)";
} else if ($subasset != 0) {
    $sql .= " where asset_idasset = ".$subasset;
} 
$linecount = $mysqli_ro->query($sql.")")->num_rows;
$sql .= " order by ".$ordercol.") limit ".($pagecount * $linesperpage).", ".$linesperpage;

//$DebugOutput .= "SearchSQL: ".$sql."<br>";

$result = $mysqli_ro->query($sql);
if (!$result) {
    echo $mysqli_ro->error;
}
$tablearray = $result->fetch_all(MYSQLI_ASSOC);


$sql_at = "SELECT * FROM `assettype` order by typename";
$sql_o = "SELECT * FROM `owner` order by username";
$sql_os = "SELECT * FROM `owningstate` order by state";
$arr_at = $mysqli_ro->query($sql_at)->fetch_all(MYSQLI_ASSOC);
$arr_o = $mysqli_ro->query($sql_o)->fetch_all(MYSQLI_ASSOC);
$arr_os = $mysqli_ro->query($sql_os)->fetch_all(MYSQLI_ASSOC);
$a_at = $standart_assettypeid;                              // Assettype default id
$a_os = $standart_owningstateid;                              // Owningstate default id
$a_o = (isset($_SESSION['ownerid']) ? $_SESSION['ownerid'] : $standart_ownerid);            // Owner default id
$a_ea = $subasset;        // Eltern id
?>

<div class="DEVNOTE">WIP: Kartenansicht</div>
<?php
if (isset($_GET['subasset'])) {
$parent_id = $subasset;
$arr_loc = array();
while ($parent_id != NULL) {
    $sql_pa = "SELECT * FROM `asset` WHERE idasset = ".$parent_id;
    $arr_pa = $mysqli_ro->query($sql_pa)->fetch_array(MYSQLI_ASSOC);
    $parent_id = $arr_pa['asset_idasset'];
    $arr_loc[] = $arr_pa;
    //echo arr_tostr($arr_loc);
}
krsort($arr_loc);
foreach($arr_loc as $loc) { ?>
    <a href="?subasset=<?php echo $loc['idasset']; ?>"><div class="locationitem" title="<?php echo $loc['location']; ?>"><?php echo $loc['name']; ?></div></a>
<?php }} ?>
<script>
    function change_page(pageindex) {
        console.debug(pageindex);
        document.getElementById('page_select').selectedIndex = pageindex;
        document.getElementById('pagech_form').submit();
    };

    function toggle_select_asset(assetid) {
        var req = new XMLHttpRequest(); 
        req.overrideMimeType("application/json");
        req.open('GET', "/functions.php?f=toggle_select_asset&assetid="+assetid, true);
        req.onload  = function() {
            var selectnav = document.getElementById('selectnav');
            selectnav.style.display = "none";
            var all_buttons = document.getElementsByClassName('text_icon_button');
            for (let i = 0; i<all_buttons.length; i++) {
                var obj = all_buttons[i];
                console.log("reset "+all_buttons[i].id);
                all_buttons[i].className = "text_icon_button unselected";
            }
            var jsonResponse = JSON.parse(req.responseText);
            console.log(jsonResponse);
            console.log("reset is done!");
            for (let i = 0; i<jsonResponse.length; i++) {
                console.log("select selectbt_id"+jsonResponse[i]);
                var obj = document.getElementById('selectbt_id'+jsonResponse[i]);
                if (obj) { obj.className = "text_icon_button selected"; }
            }
            selectnav.innerHTML = "Auswahl (" + jsonResponse.length + ") nutzen";
            selectnav.style.display = "block";
        };
        req.send(null);
    };
</script>
<table class="hidden_table">
    <tr>
    <?php if (!isset($_GET['subasset'])) { ?>
        <th align="left">
            <form>
                <input type="hidden" name="subasset" value="<?php echo $standard_subasset; ?>" />
                <input type="submit" value="Durch Räume navigieren" style="width: 100%;" />
            </form>
        </th>
    <?php }  else { ?>
        <th align="left">
            <form>
                <input type="hidden" name="page" value="assets" />
                <input type="submit" value="Alle Assets anzeigen" style="width: 100%;" />
            </form>
        </th>
        <th align="left">
            <a href="?page=asset_form&parent_assetid=<?php echo $subasset; ?>"><button class="text_icon_button" title="Hinzufügen">+</button></a>
        </th>
    <?php } ?>
        
        <td>Habe <?php echo $linecount; ?> Assets gefunden</td>
        <th align="right">
            <form>
                <input type="text" name="search" autofocus value="<?php echo $search; ?>" />
                <input type="submit" value="Suchen" />
            </form>
        </th>
    </tr>
</table>
<?php if ($show_cards) {
    foreach ($tablearray as $row)  { ?>
        <a href="?page=asset_detail&assetid=<?php echo $row['idasset']; ?>">
            <div class="assetcard">
                <header class="card_header">
                <h1><?php echo $row['name']; ?></h1>
                <?php echo $row['typename']; ?>
                </header>
                <?php if (strlen($row['description']) > 0) { ?>
                <div class="card_content"><?php echo $row['description']; ?></div>
                <?php } ?>
                <div class="card_ownerstate">
                    <div style="background-color: <?php echo $row['color1']; ?>;width:20px;height:20px;margin-right:2px;display: inline-block; border-radius: 100% 0% 0% 100%;"></div>
                    <div style="background-color: <?php echo $row['color2']; ?>;width:20px;height:20px;display: inline-block; border-radius: 0% 100% 100% 0%;"></div>
                    by <?php echo $row['username']; ?>
                </div>
            </div>
        </a>
    <?php }
} else { ?>
<table width="100%">
    <tr>
    <?php if(isset($_SESSION['ownerid'])) { ?><th></th><?php } ?>
        <th><a href="?ordercol=idasset">ID</a></th>
        <th><a href="?ordercol=name">Assetname</a></th>
        <th><a href="?ordercol=typename">Type</a></th>
        <th><a href="?ordercol=count">Menge</a></th>
        <th><a href="?ordercol=state">Ownerstate</a></th>
        <th><a href="?ordercol=ownername">Owner</a></th>
        <th colspan="4">Optionen</th>
    </tr>

    <?php if ($subasset > 0) { ?>
    <tr>
        <td colspan="10"><div class="DEVNOTE">Eintragen hier noch nicht möglich. Bitte über den Plus Button eintragen.</div></td>
    </tr>
    <form>
    <tr>
        <?php if(isset($_SESSION['ownerid'])) { ?><td></td><?php } ?>
        <td>AUTO</td>
        <td>
            <input type="hidden" name="f" value="new_asset" >
            <input type="hidden" name="a_ea" value="<?php echo $subasset; ?>" />
            <input name="a_name" required="required" value="">
            <input type="hidden" name="a_desc" value="-> QuickInput" />
            <input type="hidden" name="a_loc" value="" />
            <input type="hidden" name="a_sno" value="" />
            <input type="hidden" name="a_doc" value="" />
        </td>
        <td>
            <select name="a_at">
                <?php foreach ($arr_at as $row)  { ?>
                <option value="<?php echo $row['idassettype']; ?>" <?php echo ($a_at == $row['idassettype'] ? "selected=\"selected\"": ""); ?>><?php echo $row['typename']; ?></option>
                <?php } ?>
            </select>
        </td>
        <td><input name="a_count" type="number" value="1" min="1"></td>
        <td>
            <select name="a_os">
                <?php foreach ($arr_os as $row)  { ?>
                <option value="<?php echo $row['idowningstate']; ?>" <?php echo ($a_os == $row['idowningstate'] ? "selected=\"selected\"": ""); ?>><?php echo $row['state']; ?></option>
                <?php } ?>
            </select>
        </td>
        <td>
            <select name="a_o" id="a_o">
                <?php 
                $list_index = 0;
                foreach ($arr_o as $row)  { ?>
                <option value="<?php echo $row['idowner']; ?>" <?php echo ($a_o == $row['idowner'] ? "selected=\"selected\"": ""); ?>><?php echo "[".$row['idowner']."] ".$row['username']; ?></option>
                <?php 
                if ($standart_ownerid == $row['idowner']) $a_o_stdowner_i = $list_index;
                if ($_SESSION['ownerid'] == $row['idowner']) $a_o_owner_i = $list_index;
                $list_index++;
                } ?>
            </select>
        </td>
        <td colspan="4"><input type="submit" value="Speichern"></td>
    </tr>
    </form>
    <?php } ?>
    
    
    <?php foreach ($tablearray as $row)  { ?><tr>
        <?php if(isset($_SESSION['ownerid'])) { ?><td><button id="selectbt_id<?php echo $row['idasset']; ?>" onclick="toggle_select_asset(<?php echo $row['idasset']; ?>);" class="text_icon_button <?php echo (in_array($row['idasset'], $_SESSION['assetselectlist']) ? "selected" : "unselected"); ?>" title="Selektieren">S</button></td><?php } ?>
        <td><?php echo $row['idasset']; ?></td>
        <td><?php echo $row['name']; ?></td>
        <td><?php echo $row['typename']; ?></td>
        <td><?php echo $row['count']; ?></td>
        <td><div style="background-color: <?php echo $row['color1']; ?>;width:20px;height:20px;display: inline-block; border-radius: 100% 0% 0% 100%;"></div> <div style="background-color: <?php echo $row['color2']; ?>;width:20px;height:20px;display: inline-block; border-radius: 0% 100% 100% 0%;"></div> <?php echo $row['state']; ?></td>
        <td><?php echo "[".$row['owner_idowner']."] ".$row['username']; ?></td>
        <td>
            <?php if ($row['childpossible'] != 0) { ?>
            <a href="?subasset=<?php echo $row['idasset']; ?>"><button class="text_icon_button" title="Öffnen">></button></a>
            <?php } ?>
        </td>
        <td>
            <a href="?page=asset_detail&assetid=<?php echo $row['idasset']; ?>"><button class="text_icon_button" title="Details anzeigen">D</button></a>
        </td>
        <td>
        <?php if (isset($_SESSION['ownerid']) && ($row['owner_idowner'] == $_SESSION['ownerid'] | $_SESSION['r_e_all_assets'] == true)) { ?> 
            <a href="?page=asset_form&assetid=<?php echo $row['idasset']; ?>"><button class="text_icon_button" title="Bearbeiten">E</button></a>
            <?php } ?>
            </td>
        <td>
            <?php if ($row['childpossible'] != 0) { ?>
            <a href="?page=asset_form&parent_assetid=<?php echo $row['idasset']; ?>"><button class="text_icon_button" title="Hinzufügen">+</button></a>
            <?php } ?>
        </td>
    </tr>
    <?php } ?>

<tr>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <td colspan="4">
        <button class="text_icon_button" title="Öffnen">></button>Asset Öffnen<br>
        <button class="text_icon_button" title="Details anzeigen">D</button>Details anzeigen<br>
        <button class="text_icon_button" title="Hinzufügen">+</button>Ein Kind Asset hinzufügen<br>

    </td>
</tr>
</table>
<?php } ?>
<?php if($pagecount != 0) { ?>
<button onclick="change_page(0);">First</button>
<button onclick="change_page(<?php echo $pagecount-1; ?>);">Befor</button>
<?php } ?>
<form id="pagech_form">
    <input Type="hidden" name="ordercol" value="<?php echo $ordercol; ?>">  
    <?php if (isset($_GET['search'])) { ?><input Type="hidden" name="search" value="<?php echo $search; ?>"><?php } ?>
    <?php if (isset($_GET['subasset'])) { ?><input Type="hidden" name="subasset" value="<?php echo $subasset; ?>"><?php } ?>
    <select name="pagecount" id="page_select" onchange="this.form.submit()">
    <?php for ($i=0; $i<$linecount/$linesperpage ; $i++) { ?>
        <option value="<?php echo $i; ?>" <?php if ($pagecount == $i) { ?>selected="selected"<?php } ?>>Seite <?php echo $i+1; ?></option>
    <?php } ?>
    </select>
</form>
<?php if($pagecount != ($linecount/$linesperpage)-1) { ?>
<button onclick="change_page(<?php echo $pagecount+1; ?>);">Next</button>
<button onclick="change_page(<?php echo ($linecount/$linesperpage)-1; ?>);">Last</button>
<?php }?>