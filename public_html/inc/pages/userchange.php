<?php
$function = "ERROR- NOT ALLOW";
$u_name = "ERROR";
$u_pass = "";
$u_id = "";
$edit_username = true;
$edit_actually_pw = false;
if (isset($_SESSION['ownerid']) && !isset($_GET['uid'])) {
    $sql = "select * from owner where idowner = ".$_SESSION['ownerid'];
    $arr_u = $mysqli_ro->query($sql)->fetch_array(MYSQLI_ASSOC);
    $u_id = $arr_u['idowner'];
    $u_name = $arr_u['username'];
    $u_pass = "N0-change_PW";
    $function = "change_user";
    $edit_username = false;
    $edit_actually_pw = true;
} else if (isset($_GET['uid']) && isset($_SESSION['r_e_owner']) && $_SESSION['r_e_owner']) {
    $sql = "select * from owner where idowner = ".$_GET['uid'];
    $arr_u = $mysqli_ro->query($sql)->fetch_array(MYSQLI_ASSOC);
    $u_id = $arr_u['idowner'];
    $u_name = $arr_u['username'];
    $u_pass = "N0-change_PW";
    $function = "adm_change_user";
    $edit_username = true;
    $edit_actually_pw = false;
} else if (!isset($_SESSION['ownerid'])){
    $function = "reg_user";
    $u_name = "";
    $u_pass = "";
} 
?>
<form method="POST">
    <input type="hidden" name="f" value="<?php echo $function; ?>" />
    <input type="hidden" name="uid" value="<?php echo $u_id; ?>" />
<table>
    <!--tr>
        <th>Anzeigename</th><td><input type="showname" required="required" name="showname" value="<?php echo $u_showname; ?>" /></td>
    </tr -->
    <tr>
        <th>Benutzername</th><td><input type="uname" name="username" required="required" <?php if (!$edit_username) echo "disabled=\"disabled\""; ?> value="<?php echo $u_name; ?>" /></td>
    </tr>
    <?php if ($edit_actually_pw) { ?><tr>
        <th>aktuelles Passwort</th><td><input name="po" required="required" type="password" value="" /></td>
    </tr><?php } ?>
    <tr>
        <th>Passwort</th>
        <td>Passwort aus mindestenst 8 Zeichen und 2 Kriterien aus
            <ul>
                <li>Großbuchstaben</li>
                <li>Kleinbuchstaben</li>
                <li>Zahlen</li>
                <li>Sunderzeichen</li>
                <li>länger als 12 Ziechen</li>
            </ul>
            <b>Bitte beachten:</b> Es muss immer ein Passwort vergeben werden!
            <input name="p1" required="required" id="p1" type="password" value="<?php echo $u_pass; ?>" onkeyup="checkpw();"/><br>
            <progress id="strength" value="0" max="5"></progress></td>
    </tr>
    <tr>
        <th>Wiederholung</th><td><input id="p2" required="required" type="password" value="<?php echo $u_pass; ?>" onkeyup="checkpw();" /></td>
    </tr>
    <tr>
        <th></th><td><input name="sub" id="sub" type="submit" value="<?php echo ($function == "reg_user" ? "Benutzer erstellen" : "Benutzer ändern"); ?>" /></td>
    </tr>

</table>
</form>


<script>
    function passwordStrength(pw) {
      return /.{8,}/.test(pw) * (  /* at least 8 characters */
        /.{12,}/.test(pw)          /* bonus if longer */
        + /[a-z]/.test(pw)         /* a lower letter */
        + /[A-Z]/.test(pw)         /* a upper letter */
        + /\d/.test(pw)            /* a digit */
        + /[^A-Za-z0-9]/.test(pw)  /* a special character */
       )
    }
    
    
    var pwInput = document.getElementById("p1");
    var pwInput2 = document.getElementById("p2");
    var pwSubmit = document.getElementById("sub");
    //pwSubmit.disabled = true;

    console.log(pwInput);
    console.log(pwInput2);

    function checkpw() {
        var check_count = 0;
        document.getElementById("strength").value = passwordStrength(pwInput.value);
        console.log("PW Strength: " + passwordStrength(pwInput.value));
        if (passwordStrength(pwInput.value) >= 3) {
            pwInput.style.backgroundColor = "green";
            check_count = check_count + 1;
        } else {
            pwInput.style.backgroundColor = "red";
        }
        console.log(pwInput.value + " = " + pwInput2.value)
        if (pwInput.value == pwInput2.value) {
            pwInput2.style.backgroundColor = "green";
            check_count = check_count + 1;
        } else {
            pwInput2.style.backgroundColor = "red";
        }
        if (check_count == 2) {
            pwSubmit.disabled = false;
        } else {
            pwSubmit.disabled = true;
        }
    }

    pwInput.addEventListener('keyup', checkpw());
    pwInput2.addEventListener('keyup', checkpw());
    </script>