<?php 
$sql_og = "SELECT * FROM `ownergroup` order by `name`";
$arr_og = $mysqli_ro->query($sql_og)->fetch_all(MYSQLI_ASSOC);

$sql = "select * from owner as o join ownergroup as og on (o.ownergroup_idownergroup = og.idownergroup) order by username asc;";
$result = $mysqli_ro->query($sql);
if (!$result) {
    echo $mysqli_ro->error;
}
$tablearray = $result->fetch_all(MYSQLI_ASSOC);
?>
<script>
function set_group(uid) {
    var gidch = document.getElementById("gidch" + uid);
    var gidchr = document.getElementById("gidchr" + uid);
    var gid = gidch.value;

    var req = new XMLHttpRequest();
        req.open('GET', "/functions.php?f=usergr_ch&uid="+uid+"&gid="+gid, true);
        req.onload  = function() {
            gidchr.innerHTML = req.responseText;
        }
        req.send(null);
    console.log("UID" + uid + " GID" + gidch.value);
}
</script>
<table>
    <tr>
        <th>UserID</th>
        <th>Username</th>
        <th>Usergroup</th>
        <th>Optionen</th>
    </tr>
    <?php foreach ($tablearray as $row)  { ?>
    <tr>
        <td><?php echo $row['idowner']; ?></td>
        <td><?php echo $row['username']; ?></td>
        <td>
            <select id="gidch<?php echo $row['idowner']; ?>" onchange="set_group(<?php echo $row['idowner']; ?>)">
                <?php foreach ($arr_og as $key => $value) { ?>
                    <option value="<?php echo $value['idownergroup']; ?>" <?php echo ($value['idownergroup'] == $row['idownergroup'] ? "selected=\"selected\"" : ""); ?>><?php echo $value['name']; ?></option>
                <?php } ?>
            </select>
            <div id="gidchr<?php echo $row['idowner']; ?>"></div>
        </td>
        <td><a href="?page=userchange&uid=<?php echo $row['idowner']; ?>"><button class="text_icon_button" title="editieren">E</button></a>
            <?php if($row['blocked']) { ?>
                <a href="?f=lock_user&uid=<?php echo $row['idowner']; ?>&block=0"><button class="text_icon_button active" title="entsperren">L</button></a>
            <?php } else { ?>
                <a href="?f=lock_user&uid=<?php echo $row['idowner']; ?>&block=1"><button class="text_icon_button inactive" title="sperren">L</button></a>
            <?php } ?></td>
    </tr>
    <?php } ?>
    <tr>
        <th></th>
        <th></th>
        <th></th>
        <td>
            <button class="text_icon_button" title="editieren">E</button>Editieren eines Benutzers<br>
            <button class="text_icon_button active" title="entsperren">L</button>Benutzer entsperren<br>
            <button class="text_icon_button inactive" title="sperren">L</button>Benutzer sperren<br>
        </td>
    </tr>
</table>