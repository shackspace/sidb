<?php 
$textlength = 18; //15;
// Fremde Libs 
require($_SERVER['DOCUMENT_ROOT'].'/libs/phpqrcode/qrlib.php');
require($_SERVER['DOCUMENT_ROOT'].'/libs/fpdf/fpdf.php');

// Funktionen aus der SIDB nutzen inkl. Mysql-Verbindungen
require($_SERVER['DOCUMENT_ROOT'].'/functions.php'); 

$alllabels = false;
if (isset($_GET['all'])) $alllabels = true;
if ($alllabels | isset($_SESSION['assetselectlist']) | isset($_GET['singlelabel'])) {
$sql = "SELECT * FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype)";
$first = true;
if (!$alllabels && !isset($_GET['singlelabel'])) {
    foreach ($_SESSION['assetselectlist'] as $key => $value) {
        if ($first) {
            $sql .= " WHERE `idasset` = '".$value."'";
            $first = false;
        } else $sql .= " OR `idasset` = '".$value."'";
    }
}

if (isset($_GET['singlelabel'])) {
    $sql .= " WHERE `idasset` = '".$_GET['singlelabel']."'";
}

if($res = $mysqli_ro->query($sql))
{
    $arr = $res->fetch_all(MYSQLI_ASSOC);
} else {
    echo "ERROR [".$mysqli_ro->errno."] ".$mysqli_ro->error;
    echo "<br>".$sql;
}

$pdf = new FPDF('L','mm',array(25,25));

foreach ($arr as $value) {
    $intext = iconv('UTF-8', 'ISO-8859-1', $value['name']);
    if (strlen($intext)>$textlength) $intext = substr($intext,0,$textlength-1)."...";

    $pdf->AddPage();
    $pdf->Image('http://localhost/inc/qrcode.php?assetid='.$value['idasset'],4.8,4.8,15,15,'png');
    //$pdf->Image('http://sidb.execnet.de/img/gen_code.php?assetid='.$value['idasset'].'&width=220&height=40',0,0,90,0,'png');
    
    $pdf->SetFont('Helvetica','B',6);
    $pdf->Text(2,3,$intext);
    $pdf->Text(2,5,iconv('UTF-8', 'ISO-8859-1', $value['state']));
    $pdf->SetFont('Helvetica','',6);
    $pdf->Text(2,21,iconv('UTF-8', 'ISO-8859-1', "ID:".$value['idasset']." U:".$value['username']));
    $pdf->Text(2,23,iconv('UTF-8', 'ISO-8859-1', "SNr.:".$value['serialno']));
    $pdf->SetFont('Helvetica','B',10);
    $pdf->Text(2,8.5,'S');
    $pdf->Text(2.7,11.9,'I');
    $pdf->Text(2,15.3,'D');
    $pdf->Text(2,18.6,'B');
    $pdf->Text(20.5,8.5,'S');
    $pdf->Text(21.2,11.9,'I');
    $pdf->Text(20.5,15.3,'D');
    $pdf->Text(20.5,18.6,'B');
}


$pdf->Output();
} else { ?>
    <h1>Ähm da isch was net richtig</h1>
<?php } ?>
