<?php 
$textlength = 18; //15;
// Fremde Libs 
require($_SERVER['DOCUMENT_ROOT'].'/libs/phpqrcode/qrlib.php');
require($_SERVER['DOCUMENT_ROOT'].'/libs/fpdf/fpdf.php');

// Funktionen aus der SIDB nutzen inkl. Mysql-Verbindungen
require($_SERVER['DOCUMENT_ROOT'].'/functions.php'); 

$alllabels = false;
if (isset($_GET['all'])) $alllabels = true;
if ($alllabels | isset($_SESSION['assetselectlist']) | isset($_GET['singlelabel'])) {
$sql = "SELECT * FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype)";
$first = true;
if (!$alllabels && !isset($_GET['singlelabel'])) {
    foreach ($_SESSION['assetselectlist'] as $key => $value) {
        if ($first) {
            $sql .= " WHERE `idasset` = '".$value."'";
            $first = false;
        } else $sql .= " OR `idasset` = '".$value."'";
    }
}

if (isset($_GET['singlelabel'])) {
    $sql .= " WHERE `idasset` = '".$_GET['singlelabel']."'";
}

if($res = $mysqli_ro->query($sql))
{
    $arr = $res->fetch_all(MYSQLI_ASSOC);
} else {
    echo "ERROR [".$mysqli_ro->errno."] ".$mysqli_ro->error;
    echo "<br>".$sql;
}

$pdf = new FPDF('L','mm',array(54,70));

foreach ($arr as $value) {
    $intext = iconv('UTF-8', 'ISO-8859-1', $value['name']);
    if (strlen($intext)>$textlength) $intext = substr($intext,0,$textlength-1)."...";

    $pdf->AddPage();
    //$pdf->Image('http://localhost/img/logo_light.png',12,4,45,45,'png');
    $pdf->SetAutoPageBreak(false);
    // QR für SIDB
    $pdf->Image('http://localhost/inc/qrcode.php?assetid='.$value['idasset'],2.5,37,15,15,'png');
    $pdf->SetFont('Helvetica','',5);
    $pdf->Text(3.6,37,"Link zur SIDB:");

    $documented = false;

    if (strlen($value['docuurl'])>5) {   
        // QR zur Dokumentation
        $pdf->Image('http://localhost/inc/qrcode.php?qrtext='.urlencode($value['docuurl']),52,2.5,15,15,'png');
        $pdf->SetFont('Helvetica','',4);
        $pdf->Text(53,18.6,"DOKUMENTATION");
        $documented = true;
    }

    // Platzhalter SHACK-Logo
    $pdf->Image('http://localhost/img/logo.png',2.5,2.5,17.5,17.5,'png');
    //$pdf->Rect(2.5,2.5,17.5,17.5);


    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetXY(2.5,25);
    $pdf->MultiCell(17.5,4,iconv('UTF-8', 'ISO-8859-1', $value['idasset']),'B','C', 0);
    $pdf->SetFont('Helvetica','',5);
    $pdf->SetX(2.5);
    $pdf->Cell(17.5,3,"ASSETID");

    // Ownerstate
    $pdf->SetFont('Helvetica','B',22);
    $pdf->SetXY(20,29);
    $pdf->MultiCell(50,4,iconv('UTF-8', 'ISO-8859-1', $value['state']),0,'C', 0);
    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetXY(20,36);
    $pdf->MultiCell(50,4,iconv('UTF-8', 'ISO-8859-1', $value['printtext']),'T','C', 0);

    // Assetname
    $textlength = ($documented ? 14 : 23);
    $intext = iconv('UTF-8', 'ISO-8859-1', $value['name']);
    if (strlen($intext)>$textlength) $intext = substr($intext,0,$textlength-1)."...";
    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetXY(21,3.5);
    $pdf->MultiCell(($documented ? 31 : 47.5),6,$intext,'B','C', 0);
    $pdf->SetFont('Helvetica','',5);
    $pdf->SetX(20);
    $pdf->Cell(31,3,"ASSETNAME");
    $pdf->Ln();

    // Ownername:
    $pdf->SetFont('Helvetica','B',10);
    $pdf->SetX(21);
    $pdf->MultiCell(($documented ? 31 : 47.5),4,iconv('UTF-8', 'ISO-8859-1', $value['username']),'B','C', 0);
    $pdf->SetFont('Helvetica','',5);
    $pdf->SetX(20);
    $pdf->Cell(31,3,"OWNER");
    $pdf->Ln();

    // Serial:
    if (strlen($value['serialno'])>0) {
        $pdf->SetFont('Helvetica','B',10);
        $pdf->SetX(21);
        $pdf->MultiCell(($documented ? 31 : 47.5),4,iconv('UTF-8', 'ISO-8859-1', $value['serialno']),'B','C', 0);
        $pdf->SetFont('Helvetica','',5);
        $pdf->SetX(20);
        $pdf->Cell(31,3,"SERIALNO");
    }


    //$pdf->Image('http://sidb.execnet.de/img/gen_code.php?assetid='.$value['idasset'].'&width=220&height=40',0,0,90,0,'png');
    /*
    $pdf->SetFont('Helvetica','B',6);
    $pdf->Text(2,3,$intext);
    $pdf->Text(2,5,iconv('UTF-8', 'ISO-8859-1', $value['state']));
    $pdf->SetFont('Helvetica','',6);
    $pdf->Text(2,21,iconv('UTF-8', 'ISO-8859-1', "ID:".$value['idasset']." U:".$value['username']));
    $pdf->Text(2,23,iconv('UTF-8', 'ISO-8859-1', "SNr.:".$value['serialno']));
    $pdf->SetFont('Helvetica','B',10);
    $pdf->Text(2,8.5,'S');
    $pdf->Text(2.7,11.9,'I');
    $pdf->Text(2,15.3,'D');
    $pdf->Text(2,18.6,'B');
    $pdf->Text(20.5,8.5,'S');
    $pdf->Text(21.2,11.9,'I');
    $pdf->Text(20.5,15.3,'D');
    $pdf->Text(20.5,18.6,'B');

    */
    //$pdf->SetTextColor(255,255,255);
}


$pdf->Output();
} else { ?>
    <h1>Ähm da isch was net richtig</h1>
<?php } ?>
