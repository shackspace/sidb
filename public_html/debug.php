<?php 
if ((isset($debug) && $debug) || !isset($debug)) {
?>
<footer id=debug>
    <h1>DEBUGINFO <a href="?debug=0">OFF</a></h1>
    <h3 style="color: red">Achtung: Unverschlüsselt und alle Daten werden im Debugbereich angezeigt</h3>
    <table border="1" width="100%">
        <tr>
            <th width="25%">GET DATA</th>
            <th width="25%">POST DATA</th>
            <th width="25%">SESSION DATA</th>
            <th width="25%">Sonstiges</th>
        </tr>
        <tr>
            <td valign="top"><?php myprint_r($_GET); ?></td>
            <td valign="top"><?php myprint_r($_POST); ?></td>
            <td valign="top"><?php myprint_r($_SESSION); ?></td>
            <td valign="top">
                <h3>Debug</h3><?php myprint_r($DebugOutput); ?><br>
                <h3>JSON DATA</h3><div id="debugjson"></div>
            </td>
        </tr>
    </table>
</footer><?php
}
?>