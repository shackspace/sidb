<?php 
header('Content-Type: text/html; charset=utf-8');
include($_SERVER['DOCUMENT_ROOT'].'/functions.php'); ?>
<!DOCTYPE html>
<html>
<head>
<title>SIDB - Shack Inventory DataBase</title>
<meta name="keywords" content="Assetmanagement">
<meta name="description" content="SIDB - Shack Inventory DataBase">
<meta name="author" content="exec alias Christoph Ziegler">
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" /> 
<link rel="stylesheet" type="text/css" href="devstyle.css" /> 
<link rel="stylesheet" type="text/css" href="style_color_light.css" /> 
<!--link rel="stylesheet" type="text/css" href="style_design_screen.css" media="screen and (orientation: landscape)" /> 
<link rel="stylesheet" type="text/css" href="style_design_handheld.css" media="screen and (orientation: portrait)" /--> 
<link rel="stylesheet" type="text/css" href="style_design_screen.css" media="screen and (min-width: 1024px)" /> 
<link rel="stylesheet" type="text/css" href="style_design_handheld.css" media="screen and (max-width: 1024px)" /> 
<!--link rel="stylesheet" href="style.css" -->
<?php if(isset($_SESSION['ownerid'])) { ?>
<script type="text/javascript">
 var counter=<?php echo $sessiontimeout; ?>;
setInterval(function() { 
    if(counter>=0)
    {
        minutes = parseInt(counter / 60, 10);
        seconds = parseInt(counter % 60, 10);            
        minutes = (String(Math.abs(minutes)).length == 1) ? "0" + Math.abs(minutes) : Math.abs(minutes);
        seconds = (String(Math.abs(seconds)).length == 1) ? "0" + Math.abs(seconds) : Math.abs(seconds);
        document.getElementById("sessioncountdown").innerHTML="Deine Session läuft noch "+minutes+":"+seconds+" Min";
    }
    if (counter<60) {
        document.getElementById("sessioncountdown").style.fontWeight = "bold";
        document.getElementById("sessioncountdown").style.color = (counter % 2 == 1 ? "#f00" : "#fff");
    } else {
        document.getElementById("sessioncountdown").style.fontWeight = "normal";
        document.getElementById("sessioncountdown").style.color = "#fff";
    }
    if (counter==0) {
        window.location = "/?logout";
    }
    counter--;
}, 1000);

</script>
<?php } ?>
</head>
<body><?php include($_SERVER['DOCUMENT_ROOT'].'/inc/message.php'); ?>
<!--div id="cookieinfo">Mit dem weiteren Besuch dieser Webseite, stimmen Sie der Nutzung von Cookies zu. Diese sind nur technisch notwenig, um diese Seite zu betreiben. <a href="?page=datenschutz">DATENSCHUTZ</a></div-->
<header><h1>SIDB - Shack Inventory DataBase</h1><div class="sessioncountdown" id="sessioncountdown"><?php echo (isset($_SESSION['ownerid']) ? "Sessioninformationen werden geladen" : "Nicht angemeldet"); ?></div></header>
<div class="flex-container">
    <?php include($_SERVER['DOCUMENT_ROOT'].'/inc/nav.php'); include($_SERVER['DOCUMENT_ROOT'].'/inc/main.php');  ?>
</div>

<?php if ($debug) {
    include($_SERVER['DOCUMENT_ROOT'].'/debug.php'); 
} else {
    include($_SERVER['DOCUMENT_ROOT'].'/inc/footer.php'); 
}?>

</body>
</html>