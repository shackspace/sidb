<?php
if (session_status() !== PHP_SESSION_ACTIVE) session_start();
if (isset($_GET['logout'])) {
    //clean Session and destroy
    session_unset();
    session_destroy();
}
if (session_status() !== PHP_SESSION_ACTIVE) session_start();

// Variables for Message Outputs
$UserErrorOutput = "";
$UserInfoOutput = "";
$DebugOutput = "";

require_once($_SERVER['DOCUMENT_ROOT'].'/config.php');

if (isset($_GET['debug'])) $_SESSION['debug'] = $_GET['debug'];
if (isset($_SESSION['debug']) && $_SESSION['debug']) $debug = true;


/*$db_host =      "10.0.0.20";
$db_port =      3306;
$db_name =      "sidb";
$db_user =      "sidb";
$db_pass =      "4su8TAejQYkocWucT3YSWHSnmGpUSwFYmsiVem4AhmyYQAEPYM";
*/
$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_name, $db_port);
if ($mysqli->connect_errno) {
    die("Verbindung zur Datenbank fehlgeschlagen: ".$mysqli->connect_error);
}
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
    exit();
}

$mysqli_ro = new mysqli($db_host, $db_user_ro, $db_pass_ro, $db_name, $db_port);
if ($mysqli_ro->connect_errno) {
    die("Verbindung zur Datenbank fehlgeschlagen: ".$mysqli_ro->connect_error);
}
if (!$mysqli_ro->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli_ro->error);
    exit();
}

if (!isset($_SESSION['ownerid'])) {
    $_SESSION['r_e_owner'] = "";
    $_SESSION['r_e_owningstate'] = "";
    $_SESSION['r_e_assetcategory'] = "";
    $_SESSION['r_e_all_assets'] = "";
    $_SESSION['r_s_all_assets'] = "";
}

function arr_tostr($arr) {
    $out = "";
    if (is_array($arr)) {
        $out .= "<table border=1 cellspacing=0 cellpadding=3 width=100%>";
        $out .= '<tr><td colspan=2 style="background-color:#111;"><strong><font color=white>ARRAY</font></strong></td></tr>';
        foreach ($arr as $k => $v) {
            $out .= '<tr><td valign="top" style="width:40px;background-color:#222;">';
            $out .= '<strong>' . $k . "</strong></td><td>";
            $out .= arr_tostr($v);
            $out .= "</td></tr>";
        }
        $out .= "</table>";
        return $out;
    } 
    return $arr;
    
}

function myprint_r($my_array) {
    echo arr_tostr($my_array);
    return;
    /*if (is_array($my_array)) {
        echo "<table border=1 cellspacing=0 cellpadding=3 width=100%>";
        echo '<tr><td colspan=2 style="background-color:#111;"><strong><font color=white>ARRAY</font></strong></td></tr>';
        foreach ($my_array as $k => $v) {
                echo '<tr><td valign="top" style="width:40px;background-color:#222;">';
                echo '<strong>' . $k . "</strong></td><td>";
                myprint_r($v);
                echo "</td></tr>";
        }
        echo "</table>";
        return;
    }
    echo $my_array;*/
}


function login($username, $pwin) {
    global $mysqli_ro;
    global $DebugOutput;
    global $UserInfoOutput;
    global $UserErrorOutput;
    $mysqli=$mysqli_ro;
    //$sql = "SELECT * FROM `owner` join ownergroup on (owner.ownergroup_idownergroup = ownergroup.idownergroup) where username = '".$username."';";
    $sql = "SELECT * FROM `owner` join ownergroup on (owner.ownergroup_idownergroup = ownergroup.idownergroup) where username = ?;";
    if ($statement = $mysqli->prepare($sql))
    {
        $statement->bind_param('s', $username);
        $statement->execute();
        $result = $statement->get_result();
        //$result = $mysqli->query($sql);
        if ($row = $result->fetch_object()) {
            if (password_verify($pwin ,$row->password)) {
                if ($row->blocked != 1) {
                    // User authentificated
                    $_SESSION['ownerid'] = $row->idowner;
                    $_SESSION['r_e_owner'] = ($row->edit_owner == 1 ? true : false );
                    $_SESSION['r_e_owningstate'] = ($row->edit_assetcategory == 1 ? true : false );
                    $_SESSION['r_e_assetcategory'] = ($row->edit_owningstate == 1 ? true : false );
                    $_SESSION['r_e_all_assets'] = ($row->edit_all_assets == 1 ? true : false );
                    $_SESSION['r_s_all_assets'] = ($row->show_all_assets == 1 ? true : false );
                    return true;
                } else {
                    $DebugOutput .= "User ist gesperrt";
                    $UserErrorOutput .= "Dein Benutzer ist momentan gesperrt und der Zugriff wurde verweigert. Bitte melde dich bei einem der Administratoren.";
                    return false;
                }
            } else {
                $DebugOutput .= "PW Wrong";
                $UserErrorOutput .= "Das eingegebene Passwort oder der Benutzername ist nicht vorhanden. Bitte versuchen Sie es noch einmal";
                return false;
            }
        } else {
            $DebugOutput .= "User Wrong";
            $UserErrorOutput .= "Das eingegebene Passwort oder der Benutzername ist nicht vorhanden. Bitte versuchen Sie es noch einmal";
            return false;
        }
    }
}

function asset_log_insert($id, $action, $countchange)
{
    global $mysqli;
    global $DebugOutput;
    global $standart_ownerid;
    $sql = "INSERT INTO `sidb`.`log` (`asset_idasset`, `datetime`, `action`, `owner_idowner`, `countchange`) VALUES (?, now(), ?, ?, ?);";
    if ($statement = $mysqli->prepare($sql))  {
        $userid = (isset($_SESSION['ownerid']) ? $_SESSION['ownerid'] : $standart_ownerid);
        $DebugOutput.="Log Daten: ID:".$id.", Action:".$action.", Userid:".$userid."<br>";
        if ($statement->bind_param('isii', $id, $action, $userid, $countchange)) {
            if (!$statement->execute()) {
                $DebugOutput .= "Execute assetloginsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
            } 
        } else {
            $DebugOutput .= "Prepare assetloginsert bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
        }
    } else {
        $DebugOutput .= "Prepare assetloginsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
    }
}

//Normalize "f" input for functionselect
$formfunction = "";
if (isset($_GET['f'])) $formfunction = $_GET['f'];
if (isset($_POST['f'])) $formfunction = $_POST['f'];

//check incoming form function
switch ($formfunction) {
    case "login":
        login($_POST['u'],$_POST['p']);
        break;

    case "new_asset":
        // Input Example: ?a_name=&f=new_asset&a_id=0&a_desc=&a_loc=&a_count=1&a_at=7&a_os=1&a_o=2&a_ea=3&parent_assetid=3
        $sql = "INSERT INTO `sidb`.`asset` ";
        $sql .= "(`name`, `description`, `location`,`serialno`,`docuurl`, `assettype_idassettype`, `count`, `asset_idasset`, `ownerstate_idownerstate`, `owner_idowner`, `created`, `last-changed`) VALUES ";
        $sql .= "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now(), now());";
        if ($statement = $mysqli->prepare($sql))
        {
            if ($statement->bind_param('sssssiiiii', $_GET['a_name'], $_GET['a_desc'], $_GET['a_loc'], $_GET['a_sno'], $_GET['a_doc'], $_GET['a_at'], $_GET['a_count'], $_GET['a_ea'], $_GET['a_os'], $_GET['a_o'])) {
                if (!$statement->execute()) {
                    $DebugOutput .= "Execute assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen mit dem Hinzufügen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                } else {
                    asset_log_insert($statement->insert_id, "Asset created.", $_GET['a_count']);
                    $UserInfoOutput .= "Wurde erfolgreich eingefügt mit der ID:".$statement->insert_id;
                    if (!isset($_SESSION['assetselectlist'])) $_SESSION['assetselectlist'] = array();
                    $_SESSION['assetselectlist'][] = $statement->insert_id;
                }

            } else {
                $DebugOutput .= "Prepare assetinsert bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                $UserErrorOutput .= "Da ist was schief gelaufen mit dem Hinzufügen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
            }
        } else {
            $DebugOutput .= "Prepare assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
            $UserErrorOutput .= "Da ist was schief gelaufen mit dem Hinzufügen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
        }
        break; 

    case "edit_asset":
        // Input Example: ?a_name=assetname&f=edit_asset&a_id=3&a_desc=desc&a_loc=ort&a_count=1&a_at=2&a_os=1&a_o=1&a_ea=1&parent_assetid=1
        // SQL: UPDATE `sidb`.`asset` SET `name` = 'a', `description` = 'b', `location` = 'c', `assettype_idassettype` = '7', `count` = '2', `asset_idasset` = '8', `ownerstate_idownerstate` = '3', `owner_idowner` = '4', `deleted` = '00', `last-changed` = '2020-10-11 10:43:43' WHERE `asset`.`idasset` = 45;
        $sql_a = "SELECT a.name, a.description, a.serialno, a.location, a.docuurl, a.count, a.asset_idasset, a.deleted, a.`last-changed`, at.typename, o.username, os.state FROM `asset` as a join owningstate as os on (a.`ownerstate_idownerstate` = os.idowningstate) join owner as o on (a.`owner_idowner` = o.idowner) join assettype as at on (a.assettype_idassettype = at.idassettype) where idasset = ".$_GET['a_id'];
        $arr_a = $mysqli_ro->query($sql_a)->fetch_array(MYSQLI_ASSOC);

        $sql = "UPDATE `sidb`.`asset` SET `name` = ?, `description` = ?, `location` = ?, `serialno` = ?, `docuurl` = ?, `assettype_idassettype` = ?, `count` = ?, `asset_idasset` = ?, `ownerstate_idownerstate` = ?, `owner_idowner` = ?, `last-changed` = now() WHERE `asset`.`idasset` = ?;";
        if ($statement = $mysqli->prepare($sql))
        {
            if ($statement->bind_param('sssssiiiiii', $_GET['a_name'], $_GET['a_desc'], $_GET['a_loc'], $_GET['a_sno'], $_GET['a_doc'], $_GET['a_at'], $_GET['a_count'], $_GET['a_ea'], $_GET['a_os'], $_GET['a_o'], $_GET['a_id'])) {
                if (!$statement->execute()) {
                    $DebugOutput .= "Execute assetupdate failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                } else {
                    $logtext = "Asset updated: ";
                    $countchange = 0;
                    $arr_b = $mysqli_ro->query($sql_a)->fetch_array(MYSQLI_ASSOC);
                    $first = true;
                    foreach ($arr_a as $key => $value) {
                        if ($arr_b[$key] != $value)
                        {
                            $logtext .= ($first?"":" | ").$key.": \"".$value."\" -> \"".$arr_b[$key]."\"";
                            $first = false;
                        }
                        if ($key == "count")
                        {
                            $countchange = $arr_b[$key] - $value;
                        }
                    }

                    asset_log_insert($_GET['a_id'], $logtext, $countchange);
                    $UserInfoOutput .= $logtext;
                }

            } else {
                $DebugOutput .= "Prepare assetupdate bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
            }
        } else {
            $DebugOutput .= "Prepare assetupdate failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
            $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
        }
        break; 
        
    case "change_user":
        $sql = "select * from owner where idowner = ".$_SESSION['ownerid'];
        $arr_u = $mysqli_ro->query($sql)->fetch_array(MYSQLI_ASSOC);
        if (password_verify($_POST['po'] ,$arr_u['password'])) {
            $sql = "UPDATE `sidb`.`owner` SET `password` = ? WHERE `owner`.`idowner` = ?;";
            if ($statement = $mysqli->prepare($sql))
            {
                if ($statement->bind_param('si', password_hash($_POST['p1'], PASSWORD_ARGON2ID), $_SESSION['ownerid'])) {
                    if (!$statement->execute()) {
                        $DebugOutput .= "Execute assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                        $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                    } else {
                        $UserInfoOutput .= "Benutzerdaten wurden geändert";
                    }    
                } else {
                    $DebugOutput .= "Prepare assetinsert bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                }
            } else {
                $DebugOutput .= "Prepare assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                $UserErrorOutput .= "Da ist was schief gelaufen mit dem Hinzufügen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
            }
        } else {
            $UserErrorOutput .= "Dein aktuelles Passwort ist nicht korrekt";
        }
        break;
    case "adm_change_user":
        if ($_SESSION['r_e_owner']) {
            $sql = "select * from owner where idowner = ".$_SESSION['ownerid'];
            $arr_u = $mysqli_ro->query($sql)->fetch_array(MYSQLI_ASSOC);
            if (password_verify($_POST['po'] ,$arr_u['password'])) {
                $sql = "UPDATE `sidb`.`owner` SET `password` = ? WHERE `owner`.`idowner` = ?;";
                if ($statement = $mysqli->prepare($sql))
                {
                    if ($statement->bind_param('si', password_hash($_POST['p1'], PASSWORD_ARGON2ID), $_SESSION['ownerid'])) {
                        if (!$statement->execute()) {
                            $DebugOutput .= "Execute assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                            $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                        } else {
                            $UserInfoOutput .= "Benutzerdaten wurden geändert";
                        }    
                    } else {
                        $DebugOutput .= "Prepare assetinsert bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                        $UserErrorOutput .= "Da ist was schief gelaufen mit dem Update. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                    }
                } else {
                    $DebugOutput .= "Prepare assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen mit dem Hinzufügen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                }
            } else {
                $UserErrorOutput .= "Dein aktuelles Passwort ist nicht korrekt";
            }
        } else {
            $UserErrorOutput .= "Das hast du kleiner Stricher wohl gedacht!";
        }
        break;
    case "reg_user":
        $sql_o = "SELECT * FROM `owner` where username = '".$_POST['username']."'";
        $count_o = $mysqli_ro->query($sql_o)->num_rows;
        $DebugOutput .= "User found with name ".$_POST['username'].": ".$count_o;
        if ($count_o > 0) {
            $UserErrorOutput .= "Diese Benutzername existiert bereits";
        } else {
            $sql = "INSERT INTO `sidb`.`owner` (`username`, `password`, `ownergroup_idownergroup`, `blocked`) VALUES (?,?,?,?);";
            if ($statement = $mysqli->prepare($sql))
            {
                if ($statement->bind_param('ssii', $_POST['username'], password_hash($_POST['p1'], PASSWORD_ARGON2ID), $standart_ownergroupid, $reguser_blocked)) {
                    if (!$statement->execute()) {
                        $DebugOutput .= "Execute failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                        $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                    } else {
                        $UserInfoOutput .= "Benutzer wurde angelegt.";
                        if ($reguser_blocked) $UserInfoOutput .= " Der Benutzer ist noch nicht freigegeben und muss von einem der Administratoren freigegeben werden";
                    }    
                } else {
                    $DebugOutput .= "Prepare bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                }
            } else {
                $DebugOutput .= "Prepare failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
            }
        }
        break;

    case "lock_user":
        $sql = "UPDATE `sidb`.`owner` SET `blocked` = ? WHERE `owner`.`idowner` = ?;";
        if (isset($_GET['block']) && isset($_GET['uid'])) {
            if ($statement = $mysqli->prepare($sql))
            {
                if ($statement->bind_param('ii', $_GET['block'], $_GET['uid'])) {
                    if (!$statement->execute()) {
                        $DebugOutput .= "Execute assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                        $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                    }    
                } else {
                    $DebugOutput .= "Prepare assetinsert bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                }
            } else {
                $DebugOutput .= "Prepare assetinsert failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
            }
        } else {
            $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
        }
        break;

    case "json_asset_childposible":
        $output = array();
        $eltern_id = $standard_subasset;
        if (isset($_GET['id'])) $eltern_id = $_GET['id'];
        $output = array('currentid' => $eltern_id);
        $sql_c = "SELECT *  FROM `asset` as a join assettype as at on (a.assettype_idassettype = at.idassettype) WHERE at.childpossible = 1 AND a.`asset_idasset` = ".$eltern_id;
        $arr_c = $mysqli_ro->query($sql_c)->fetch_all(MYSQLI_ASSOC);

        $parent_id = $eltern_id;
        $arr_loc = array();
        while ($parent_id != NULL) {
            $sql_pa = "SELECT * FROM `asset` WHERE idasset = ".$parent_id;
            $arr_pa = $mysqli_ro->query($sql_pa)->fetch_array(MYSQLI_ASSOC);
            $parent_id = $arr_pa['asset_idasset'];
            $arr_loc[] = $arr_pa;
            //echo arr_tostr($arr_loc);
        }

        //krsort($arr_loc);

        $output = array(
            'currentid' => $eltern_id,
            'childs' => $arr_c,
            'parents' => $arr_loc
        );
        
    case "usergr_ch":
        if ($_SESSION['r_e_owner'] && isset($_GET['uid']) && isset($_GET['gid'])) {
            $sql = "UPDATE `owner` SET `ownergroup_idownergroup` = ? WHERE `owner`.`idowner` = ?;";
            if ($statement = $mysqli->prepare($sql))
            {
                if ($statement->bind_param('ii', $_GET['gid'], $_GET['uid'])) {
                    if (!$statement->execute()) {
                        $DebugOutput .= "Execute Usergroupupdate failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                        $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                        echo "ERROR";
                    } else {
                        echo "geändert";
                    } 
                } else {
                    $DebugOutput .= "Prepare Usergroupupdate bind failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                    $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                    echo "ERROR";
                }
            } else {
                $DebugOutput .= "Prepare Usergroupupdate failed: [".$mysqli->errno."] ".$mysqli->error." <br>";
                $UserErrorOutput .= "Da ist was schief gelaufen. Folgende Parameter wurden übergeben:".arr_tostr($_GET);
                echo "ERROR";
            }
        } else echo "ERROR:MISS DATA";
        break;

        echo json_encode($output);
        break;
    case "select_asset":
        if (!isset($_SESSION['assetselectlist'])) $_SESSION['assetselectlist'] = array();
        if (!in_array($_GET['assetid'], $_SESSION['assetselectlist'])) $_SESSION['assetselectlist'][] = $_GET['assetid'];
        break;

    case "deselect_asset":
        if (in_array($_GET['assetid'], $_SESSION['assetselectlist'])) array_splice($_SESSION['assetselectlist'],array_search($_GET['assetid'],$_SESSION['assetselectlist']),1);
        if (count($_SESSION['assetselectlist'])<1) unset($_SESSION['assetselectlist']);
        break;

    case "toggle_select_asset":
        if (!isset($_SESSION['assetselectlist'])) $_SESSION['assetselectlist'] = array();
        if (in_array($_GET['assetid'], $_SESSION['assetselectlist'])) {
            array_splice($_SESSION['assetselectlist'],array_search($_GET['assetid'],$_SESSION['assetselectlist']),1);
        } else {
            $_SESSION['assetselectlist'][] = $_GET['assetid'];
        }
        if (count($_SESSION['assetselectlist'])<1) { 
            unset($_SESSION['assetselectlist']); 
        }
        if (!isset($_GET['hideoutput'])) echo json_encode($_SESSION['assetselectlist']);
        break;

    case "deselect_all_assets":
        unset($_SESSION['assetselectlist']);
        break;

    case "":
        $sql = "UPDATE `sidb`.`asset` SET `name` = 'testnotlogin1', `description` = 'nicht angemeldet1' WHERE `asset`.`idasset` = 38;";
        break;

    default:
        $DebugOutput .= "Formfuction not found";
        $UserErrorOutput .= "Da ist was schief gelaufen. Die Function \"".$formfunction."\" ist nicht vorhanden";
        break;    
}

