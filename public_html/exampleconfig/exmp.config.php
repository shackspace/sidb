<?php
// Database Credentials
$db_host =      "localhost";
$db_port =      3306;
$db_name =      "sidb";

// Zugang mit schreibrechten
$db_user =      "sidb";
$db_pass =      "ganz sicheres Passwort";
// Zugang fuer einen Read Only user (nur SELECT)
$db_user_ro =      "sidb_ro";
$db_pass_ro =      "sicheres Passwort";

// Debugmode on by Sessionstart
$debug = false;

// ID from Main Asset
$standard_subasset = 1;

$standart_ownerid = 1;          // SHACK
$standart_owningstateid = 1;    // Public
$standart_assettypeid = 10;     // Undefined

$standart_ownergroupid = 2;

$reguser_blocked = true;

$sessiontimeout = 1200;

$serverurl = "http://sidb.execnet.de";
//$sessiontimeout = 30;
